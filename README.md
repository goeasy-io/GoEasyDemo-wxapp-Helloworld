## 本项目已迁移至新的地址，本项目停止维护
[前往全新GoEasy Websocket消息推送源码下载和在线体验](https://www.goeasy.io/cn/demos/demos.html#helloworld)



# 原生微信小程序使用GoEasy快速实现websocket实时通讯

### 小程序注意事项
如果开发微信小程序，需要登录到微信官方后台，将wx-hangzhou.goeasy.io加入到socket合法域名列表

### 替换您的appkey
打开app.js，找到初始化GoEasy的地方，将appkey替换成您自己的appkey  


### 还没有appkey？  
1. 请先去[GoEasy官网](https://www.goeasy.io)进行注册
2. 创建一个应用
3. 在应用详情页面即可查看到appkey

### 体验
建议可以同时在小程序开发工具和手机上同时运行，体验多个客户端之间互动。

### 开发者文档
[小程序集成GoEasy教程](https://docs.goeasy.io/2.x/pubsub/get-start)